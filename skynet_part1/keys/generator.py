import os
from Crypto.PublicKey import RSA

if __name__ == "__main__":
    private = RSA.generate(2048)
    private_key = private.exportKey('PEM')
    public = private.publickey()
    public_key = public.exportKey('PEM')

    f = open('private_key','w')
    f.write(private_key.decode(encoding='UTF-8'))
    f.close()    
    f = open('public_key','w')
    f.write(public_key.decode(encoding='UTF-8'))
    f.close()