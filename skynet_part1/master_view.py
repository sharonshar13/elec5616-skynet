import os
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES

def decrypt_valuables(f):
    # TODO: For Part 2, you'll need to decrypt the contents of this file
    # The existing scheme uploads in plaintext
    # As such, we just convert it back to ASCII and print it out

    # Get AES key and IV and encrypted data
    aes_key_iv = f[:256]
    encrypted_data = f[256:]

    # Use PKCS1_OAEP to decrypt AES key and IV
    ras_private_key = RSA.importKey(open('keys/private_key').read())
    rsa_cipher = PKCS1_OAEP.new(ras_private_key)
    decrypted_aes_key_iv = rsa_cipher.decrypt(aes_key_iv)

    # Get AES key and IV
    aes_key = decrypted_aes_key_iv[:32]
    aes_iv = decrypted_aes_key_iv[32:]

    # Use AES to decrypt data
    aes_cipher = AES.new(aes_key, AES.MODE_CFB, aes_iv)
    decrypted_data = aes_cipher.decrypt(encrypted_data)
    print(str(decrypted_data, 'ascii'))


if __name__ == "__main__":
    fn = input("Which file in pastebot.net does the botnet master want to view? ")
    if not os.path.exists(os.path.join("pastebot.net", fn)):
        print("The given file doesn't exist on pastebot.net")
        os.exit(1)
    f = open(os.path.join("pastebot.net", fn), "rb").read()
    decrypt_valuables(f)
