import os
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Hash import SHA
from Crypto.Signature import PKCS1_PSS

# Instead of storing files on disk,
# we'll save them in memory for simplicity
filestore = {}
# Valuable data to be sent to the botmaster
valuables = []

###

def save_valuable(data):
    valuables.append(data)

def encrypt_for_master(data):
    # Encrypt the file so it can only be read by the bot master
    # TODO: For Part 2: encryt it using rsa
    
    # Get randome IV and 32 bit AES key
    iv = Random.new().read(AES.block_size)
    aes_key = Random.new().read(32)
    
    # Use AES to encrypt original data
    aes_cipher = AES.new(aes_key, AES.MODE_CFB, iv)
    encrypted_data = aes_cipher.encrypt(data)

    # Use PKCS1_OAEP to encrypt AES key and IV
    rsa_public_key = RSA.importKey(open('keys/public_key').read())
    rsa_cipher = PKCS1_OAEP.new(rsa_public_key)
    aes_key_iv = rsa_cipher.encrypt(aes_key + iv)

    # Add RSA encrypted AES key and IV to the begining of data
    final_data = aes_key_iv + encrypted_data

    return final_data

def upload_valuables_to_pastebot(fn):
    # Encrypt the valuables so only the bot master can read them
    valuable_data = "\n".join(valuables)
    valuable_data = bytes(valuable_data, "ascii")
    encrypted_master = encrypt_for_master(valuable_data)

    # "Upload" it to pastebot (i.e. save in pastebot folder)
    f = open(os.path.join("pastebot.net", fn), "wb")
    f.write(encrypted_master)
    f.close()

    print("Saved valuables to pastebot.net/%s for the botnet master" % fn)

###

def verify_file(f):
    # Verify the file was sent by the bot master
    # TODO: For Part 2, you'll use public key crypto here

    # Get signature and original data
    signature = f[:256] 
    data = f[256:]

    # Import RSA public key
    rsa_public_key = RSA.importKey(open('keys/public_key').read())

    # Hash original data
    hash = SHA.new()
    hash.update(data)

    # Use PKCS1_PSS to verify signature
    signature_cipher = PKCS1_PSS.new(rsa_public_key)
    is_valid = signature_cipher.verify(hash, signature)

    return is_valid

def process_file(fn, f):
    if verify_file(f):
        # If it was, store it unmodified
        # (so it can be sent to other bots)
        # Decrypt and run the file
        filestore[fn] = f
        print("Stored the received file as %s" % fn)
    else:
        print("The file has not been signed by the botnet master")

def download_from_pastebot(fn):
    # "Download" the file from pastebot.net
    # (i.e. pretend we are and grab it from disk)
    # Open the file as bytes and load into memory
    if not os.path.exists(os.path.join("pastebot.net", fn)):
        print("The given file doesn't exist on pastebot.net")
        return
    f = open(os.path.join("pastebot.net", fn), "rb").read()
    process_file(fn, f)

def p2p_download_file(sconn):
    # Download the file from the other bot
    fn = str(sconn.recv(), "ascii")
    f = sconn.recv()
    print("Receiving %s via P2P" % fn)
    process_file(fn, f)

###

def p2p_upload_file(sconn, fn):
    # Grab the file and upload it to the other bot
    # You don't need to encrypt it only files signed
    # by the botnet master should be accepted
    # (and your bot shouldn't be able to sign like that!)
    if fn not in filestore:
        print("That file doesn't exist in the botnet's filestore")
        return
    print("Sending %s via P2P" % fn)
    sconn.send(fn)
    sconn.send(filestore[fn])

def run_file(f):
    # If the file can be run,
    # run the commands
    pass
