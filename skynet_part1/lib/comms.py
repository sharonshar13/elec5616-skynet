import struct

from Crypto.Cipher import AES
from Crypto.Hash import HMAC
from ctypes import c_uint32

from dh import create_dh_key, calculate_dh_secret

class StealthConn(object):
    def __init__(self, conn, client=False, server=False, verbose=False):
        self.conn = conn
        self.cipher = None
        self.client = client
        self.server = server
        self.verbose = verbose
        self.shared_hash = None
        self.message_count = 0
        self.initiate_session()

    def initiate_session(self):
        # Perform the initial connection handshake for agreeing on a shared secret

        ### TODO: Your code here!
        # This can be broken into code run just on the server or just on the client
        if self.server or self.client:
            my_public_key, my_private_key = create_dh_key()
            # Send them our public key
            self.send(bytes(str(my_public_key), "ascii"))
            # Receive their public key
            their_public_key = int(self.recv())
            # Obtain our shared secret
            self.shared_hash = bytes.fromhex(calculate_dh_secret(their_public_key, my_private_key))
            print("Shared hash: {}".format(self.shared_hash))

        # Use AES: key(256 bits =  32 bytes), iv(128 bits = 16 bytes)
        self.cipher = AES.new(self.shared_hash[:32], AES.MODE_CFB, self.shared_hash[:16])

    def send(self, data):
        
        if self.shared_hash != None:
            # Append HMAC
            hmac = HMAC.new(self.shared_hash)
            hmac.update(data)
            data = bytes(data.decode("ascii") + hmac.hexdigest(),"ascii")
            
            # Append count
            self.message_count = self.message_count + 1
            data = bytes(data.decode("ascii") + "{:08d}".format(self.message_count), "ascii")

        if self.cipher:
            encrypted_data = self.cipher.encrypt(data)
            if self.verbose:
                print("Original data (With MAC): {}".format(data))
                print("Encrypted data: {}".format(repr(encrypted_data)))
                print("Sending packet of length {}".format(len(encrypted_data)))
        else:
            encrypted_data = data

        # Encode the data's length into an unsigned two byte int ('H')
        pkt_len = struct.pack('H', len(encrypted_data))
        self.conn.sendall(pkt_len)
        self.conn.sendall(encrypted_data)

    def recv(self):
        # Decode the data's length from an unsigned two byte int ('H')
        pkt_len_packed = self.conn.recv(struct.calcsize('H'))
        unpacked_contents = struct.unpack('H', pkt_len_packed)
        pkt_len = unpacked_contents[0]

        encrypted_data = self.conn.recv(pkt_len)
        if self.cipher:
            data = self.cipher.decrypt(encrypted_data)
            if self.verbose:
                print("Receiving packet of length {}".format(pkt_len))
                print("Encrypted data: {}".format(repr(encrypted_data)))
                print("Original data (With MAC): {}".format(data))
        else:
            data = encrypted_data

        if self.shared_hash != None:
            # Delete & Validate Count
            count = int(data[len(data) - 8:])
            data = data[:len(data) - 8]
            if count <= self.message_count:
                raise RuntimeError("Count is not valid")
            else:
                self.message_count = count

            # Delete & Validate HMAC
            hmac = HMAC.new(self.shared_hash)
            hmac_data = data[len(data) - hmac.digest_size * 2:]
            data = data[:len(data) - hmac.digest_size * 2]
            hmac.update(data)
            if hmac.hexdigest() != str(hmac_data,"ascii"): 
                raise RuntimeError("Message is not valid")
        
        return data

    def close(self):
        self.conn.close()
